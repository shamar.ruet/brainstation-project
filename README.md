# project for brain station interview
#Here i use spring for backend and angular 6 for frontend and oracle database
- This project is a simple social networking website of an Angular6 frontend application with a simple Spring Boot backend API with Spring  Security.
# Things to do for back end 
- import the project from your IDE
- Please check the application.properties file 
- Now Enter Your Oracle connection username and password
- run the program
# Things to do for front end 
- import the project from your IDE
- go to the file location
- write the command npm install
- write ng serve
-the project will open at http://localhost:4200 
