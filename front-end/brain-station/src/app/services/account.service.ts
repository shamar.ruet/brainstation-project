import { Injectable } from '@angular/core';
import {User} from "../model/model.user";
import { HttpClient, HttpHeaders} from '@angular/common/http';
const httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})
};


@Injectable({
  providedIn: 'root'
})
export class AccountService {
  Basepath: any ='http://localhost:8070'; 

  constructor(public http: HttpClient) { }
  createAccount(user:User){
    return this.http.post(this.Basepath + 'account/register', user, httpOptions).subscribe(
      data => {
      });
  }
}
