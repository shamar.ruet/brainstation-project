import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders , HttpParams} from '@angular/common/http';
import {User} from "../model/model.user";
const httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  Basepath: any ='http://localhost:8070'; 

  constructor(public http: HttpClient) { }


  public logIn(user: User){


    return this.http.get( this.Basepath +"/account/login" ,   httpOptions)
      .subscribe((response: Response) => {
      let user = response.json().principal;
      if (user) {
        localStorage.setItem('currentUser', JSON.stringify(user));
        localStorage.setItem('currentUserId', JSON.stringify(user));
      }
    });
  }
  logOut() {
    return this.http.post(this.Basepath +"logout",{})
      .subscribe((response: Response) => {
        localStorage.removeItem('currentUser');
      });

  }
}
