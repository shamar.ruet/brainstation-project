import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {HttpClient, HttpHeaders} from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  Basepath: any ='http://localhost:8070'; 
  locationList: any;
  statusList: any;
  allStatusList: any;
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getLocation();
    this.statusType();
    this.getAllStatus();
  }
  getLocation() {
    this.http.get( this.Basepath + '/api/getAllLocation').subscribe(response => {
      this.locationList = response;
    });
  }
  statusType() {
    this.statusList = [{id: '0', name: 'private'}, {id: '1', name: 'public'}];
  }
  getAllStatus() {
    this.http.get( this.Basepath + '/api/getAllLocation').subscribe(response => {
      this.allStatusList = response;
    });
  }
  save(contentEntry: NgForm) {
    const id = localStorage.getItem('currentUserId');
    const location = contentEntry.controls['location'].value;
    const statustype = contentEntry.controls['statusType'].value;
    const status = contentEntry.controls['status'].value;
    this.http.post(this.Basepath + '/api/post/timeline', {id , location , statustype , status }, httpOptions).subscribe(
      data => {
        this.getAllStatus();
      });
  }

}
