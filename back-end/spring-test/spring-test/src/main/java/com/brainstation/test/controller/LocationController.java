package com.brainstation.test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.brainstation.test.model.Location;
import com.brainstation.test.services.LocationService;


@RestController
@CrossOrigin
@RequestMapping("/api") 
public class LocationController {
	@Autowired
	LocationService locationService;

	@GetMapping("/getAllLocation")
	public List<Location>getAllSemesters() {
		return locationService.getAllLocation();
	}
	
}