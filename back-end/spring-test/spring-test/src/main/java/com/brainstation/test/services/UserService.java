package com.brainstation.test.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brainstation.test.dao.UserRepository;
import com.brainstation.test.model.UserInfo;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	public UserInfo save(UserInfo user) {
		return userRepository.saveAndFlush(user);
	}

	public UserInfo update(UserInfo user) {
		return userRepository.save(user);
	}

	public UserInfo find(String userName) {
		return userRepository.findOneByUsername(userName);
	}


}
