package com.brainstation.test.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="location")
public class Location {
    @Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LOCATION_SEQ")
	@SequenceGenerator(name = "LOCATION_SEQ", sequenceName = "location_sequence", allocationSize = 1)
	private Integer id;
	private String name;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Location(Integer id, String name) {
	
		this.id = id;
		this.name = name;
	}
	public Location() {
	
	}
	@Override
	public String toString() {
		return "Location [id=" + id + ", name=" + name + "]";
	}
	
	
	
}

