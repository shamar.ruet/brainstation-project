package com.brainstation.test.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brainstation.test.dao.TimelineRepository;
import com.brainstation.test.model.Timeline;




@Service
public class TimelineService {
	@Autowired
	TimelineRepository timelineRepository;
	
	
	  public Timeline saveOrUpdate(Timeline timeline) {
		  timelineRepository.save(timeline);
	        return (Timeline) timelineRepository;
	    }
	    public List<Timeline> getAllInfo() {
	        
			return (ArrayList<Timeline>)timelineRepository.findAll();
		}
	 	public void deleteStatus(Integer id) {
	 		timelineRepository.deleteById(id);
	  	}
	  	public Timeline updateStatus(Timeline timeline) {
	  		return timelineRepository.save(timeline);
	  	}
	  	
}
