package com.brainstation.test.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brainstation.test.dao.LocationRepository;
import com.brainstation.test.model.Location;

@Service
public class LocationService {
	@Autowired
	LocationRepository locationRepository;

    public List<Location> getAllLocation() {
        
		return (List<Location>)locationRepository.findAll();
	}
}
