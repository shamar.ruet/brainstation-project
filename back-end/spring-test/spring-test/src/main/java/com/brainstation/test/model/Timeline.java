package com.brainstation.test.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="timeline")
public class Timeline {
	private Integer id;
	private String location;
	private Integer statustype;
	private String status;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Integer getStatustype() {
		return statustype;
	}
	public void setStatustype(Integer statustype) {
		this.statustype = statustype;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Timeline(Integer id, String location, Integer statustype, String status) {
		
		this.id = id;
		this.location = location;
		this.statustype = statustype;
		this.status = status;
	}
	public Timeline() {
	
	}
	@Override
	public String toString() {
		return "Timeline [id=" + id + ", location=" + location + ", statustype=" + statustype + ", status=" + status
				+ "]";
	}
	

}
