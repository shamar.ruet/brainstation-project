package com.brainstation.test.dao;

import java.util.List;

import javax.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.brainstation.test.model.Location;


@Transactional
@Repository
public interface LocationRepository extends CrudRepository<Location, Integer> {
	List<Location>findAll();
//	void deleteById(Integer id);
//	void save(SemesterArchive semesterArchive);
	

}
