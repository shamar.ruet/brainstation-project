package com.brainstation.test.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.brainstation.test.model.Timeline;



public interface TimelineRepository extends CrudRepository<Timeline, Integer> {
	List<Timeline>findAll();
	void deleteById(Integer id);
}
