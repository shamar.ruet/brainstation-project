package com.brainstation.test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.brainstation.test.model.Timeline;
import com.brainstation.test.services.TimelineService;



@RestController
@CrossOrigin
@RequestMapping("/api")  
public class TimelineController {
	@Autowired
	TimelineService timelineService;
	
	
	@PostMapping("/post/timeline")
	public Timeline createTimeline(@RequestBody Timeline timeline) {
		return timelineService.saveOrUpdate(timeline);
	}
	@GetMapping("/get/timeline/info")
	public List<Timeline>getAllInfo() {
		return timelineService.getAllInfo();
	}
	
	@DeleteMapping("/delete/status/{id}")
	public void deleteStatus(@PathVariable Integer id) {
		timelineService.deleteStatus(id); 
	}
		
	
	@PutMapping("/update/status")
	public Timeline updateStatus(@RequestBody Timeline timeline) {
	   return  timelineService.updateStatus(timeline);
	}
	

}
