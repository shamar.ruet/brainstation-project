package com.brainstation.test.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.brainstation.test.model.UserInfo;

@Repository
public interface UserRepository extends JpaRepository<UserInfo, Long> {

public UserInfo findOneByUsername(String username);
}
